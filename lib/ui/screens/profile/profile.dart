import 'package:eceb/ui/widgets/home/appbar.dart';
import 'package:eceb/ui/widgets/profile/account_details.dart';
import 'package:eceb/ui/widgets/profile/activity.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Appbar(
              title: "ECEB",
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: displayWidth(context) * 0.06,
                vertical: displayWidth(context) * 0.08,
              ),
              child: AccountDetails(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: displayWidth(context) * 0.04,
                // vertical: displayWidth(context) * 0.08,
              ),
              child: Activity(),
            ),
            SizedBox(
              height: displayHeight(context) * 0.04,
            ),
          ],
        ),
      ),
    );
  }
}
