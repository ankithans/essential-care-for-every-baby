import 'package:eceb/ui/widgets/home/appbar.dart';
import 'package:eceb/ui/widgets/list_babies/past_registered.dart';
import 'package:eceb/ui/widgets/list_babies/recently_added.dart';
import 'package:eceb/ui/widgets/list_babies/search_bar.dart';
import 'package:eceb/ui/widgets/list_babies/sort_dropdown.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';

class ListBabies extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Appbar(
              title: "List of Babies",
            ),
            Row(
              children: [
                SizedBox(
                  width: displayWidth(context) * 0.03,
                ),
                SearchBar(),
                SortDropDown(),
              ],
            ),
            SizedBox(
              height: displayHeight(context) * 0.01,
            ),
            RecentlyAdded(),
            SizedBox(
              height: displayHeight(context) * 0.013,
            ),
            PastRegistered(),
          ],
        ),
      ),
    );
  }
}
