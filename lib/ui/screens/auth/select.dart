import 'package:eceb/ui/widgets/auth/auth_header.dart';
import 'package:eceb/ui/widgets/auth/footer.dart';
import 'package:eceb/ui/widgets/auth/select_button.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';

class Select extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          AuthHeader(),
          SizedBox(
            height: displayHeight(context) * 0.07,
          ),
          SelectButton(
            title: "Individual",
            onPressed: () {
              Navigator.pushNamed(context, '/loginindividual');
            },
          ),
          SizedBox(
            height: displayHeight(context) * 0.023,
          ),
          SelectButton(
            title: "  Facility    ",
            onPressed: () {
              Navigator.pushNamed(context, '/facility');
            },
          ),
          SizedBox(
            height: displayHeight(context) * 0.1,
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 50,
            ),
            child: Footer(),
          ),
        ],
      ),
    );
  }
}
