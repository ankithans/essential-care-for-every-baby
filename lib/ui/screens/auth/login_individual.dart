import 'package:eceb/ui/widgets/auth/auth_header_login.dart';
import 'package:eceb/ui/widgets/auth/footer.dart';
import 'package:eceb/ui/widgets/auth/signin_signup_button.dart';
import 'package:eceb/ui/widgets/auth/text_box.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';

class LoginIndividual extends StatelessWidget {
  TextEditingController _employeeCodeController;
  TextEditingController _passwordController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            AuthHeaderLogin(),
            SizedBox(
              height: displayHeight(context) * 0.045,
            ),
            TextBox(
              placeholder: "Enter Employee Code",
              textInputType: TextInputType.text,
              textEditingController: _employeeCodeController,
              obscure: false,
            ),
            TextBox(
              placeholder: "Enter the Password",
              textInputType: TextInputType.visiblePassword,
              textEditingController: _passwordController,
              obscure: true,
            ),
            SizedBox(
              height: displayHeight(context) * 0.01,
            ),
            SigninSignupButton(
              title: "Sign In",
              onPressed: () {
                Navigator.pushNamed(context, '/homepage');
              },
            ),
            SizedBox(
              height: displayHeight(context) * 0.07,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 50,
              ),
              child: Footer(),
            ),
          ],
        ),
      ),
    );
  }
}
