import 'package:eceb/ui/widgets/home/details_text_box.dart';
import 'package:eceb/utils/colors_helpers.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:chips_choice/chips_choice.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  int tag = 1;
  List<String> options = [
    'Singleton',
    'Multiple',
  ];

  @override
  Widget build(BuildContext context) {
    return Material(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: primaryColor,
          body: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                            size: 22,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        SizedBox(
                          width: displayWidth(context) * 0.02,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: displayHeight(context) * 0.04,
                            ),
                            Text(
                              "Registration",
                              style: GoogleFonts.poppins(
                                color: Colors.white,
                                fontSize: 28,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              " ID: ****124",
                              style: GoogleFonts.poppins(
                                color: Colors.white,
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: displayHeight(context) * 0.015),
                      child: Image.asset(
                        "assets/images/logo.png",
                        width: 60,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: displayHeight(context) * 0.02,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                  ),
                  padding: EdgeInsets.all(0),
                  child: Column(
                    children: [
                      Container(
                        width: displayWidth(context),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(20),
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: displayWidth(context) * 0.08,
                            vertical: displayHeight(context) * 0.02,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Mother's Details",
                                style: GoogleFonts.poppins(
                                  color: Colors.grey[800],
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              SizedBox(
                                height: displayHeight(context) * 0.01,
                              ),
                              DetailsTextBox(
                                obscure: false,
                                placeholder: "Mother's Name",
                                textInputType: TextInputType.text,
                              ),
                              DetailsTextBox(
                                obscure: false,
                                placeholder: "Ward Name",
                                textInputType: TextInputType.text,
                              ),
                              SizedBox(
                                height: displayHeight(context) * 0.02,
                              ),
                              Text(
                                "Babies delivered",
                                style: GoogleFonts.poppins(
                                  color: Colors.grey[800],
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              ChipsChoice<int>.single(
                                padding: EdgeInsets.only(
                                    top: displayHeight(context) * 0.004),
                                choiceActiveStyle: C2ChoiceStyle(),
                                choiceStyle: C2ChoiceStyle(
                                    borderRadius: BorderRadius.circular(5),
                                    padding: EdgeInsets.symmetric(
                                      horizontal: displayWidth(context) * 0.07,
                                      vertical: displayHeight(context) * 0.015,
                                    ),
                                    // elevation: 2,
                                    // borderColor: Colors.white,
                                    labelStyle: GoogleFonts.poppins(
                                      color: Colors.grey[800],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                    )),
                                value: tag,
                                onChanged: (val) => setState(() => tag = val),
                                choiceItems: C2Choice.listFrom<int, String>(
                                  source: options,
                                  value: (i, v) => i,
                                  label: (i, v) => v,
                                ),
                              ),
                              Divider(),
                              SizedBox(
                                height: displayHeight(context) * 0.02,
                              ),
                              Text(
                                "Mode of Delivery",
                                style: GoogleFonts.poppins(
                                  color: Colors.grey[800],
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              ChipsChoice<int>.single(
                                padding: EdgeInsets.only(
                                    top: displayHeight(context) * 0.004),
                                choiceActiveStyle: C2ChoiceStyle(),
                                choiceStyle: C2ChoiceStyle(
                                    borderRadius: BorderRadius.circular(5),
                                    padding: EdgeInsets.symmetric(
                                      horizontal: displayWidth(context) * 0.07,
                                      vertical: displayHeight(context) * 0.015,
                                    ),
                                    // elevation: 2,
                                    // borderColor: Colors.white,
                                    labelStyle: GoogleFonts.poppins(
                                      color: Colors.grey[800],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                    )),
                                value: tag,
                                onChanged: (val) => setState(() => tag = val),
                                choiceItems: C2Choice.listFrom<int, String>(
                                  source: options,
                                  value: (i, v) => i,
                                  label: (i, v) => v,
                                ),
                              ),
                              Divider(),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: displayHeight(context) * 0.03,
                      ),
                      Container(
                        width: displayWidth(context),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(20),
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: displayWidth(context) * 0.08,
                            vertical: displayHeight(context) * 0.02,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Mother's Details",
                                style: GoogleFonts.poppins(
                                  color: Colors.grey[800],
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              SizedBox(
                                height: displayHeight(context) * 0.01,
                              ),
                              DetailsTextBox(
                                obscure: false,
                                placeholder: "Mother's Name",
                                textInputType: TextInputType.text,
                              ),
                              DetailsTextBox(
                                obscure: false,
                                placeholder: "Ward Name",
                                textInputType: TextInputType.text,
                              ),
                              SizedBox(
                                height: displayHeight(context) * 0.02,
                              ),
                              Text(
                                "Babies delivered",
                                style: GoogleFonts.poppins(
                                  color: Colors.grey[800],
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              ChipsChoice<int>.single(
                                padding: EdgeInsets.only(
                                    top: displayHeight(context) * 0.004),
                                choiceActiveStyle: C2ChoiceStyle(),
                                choiceStyle: C2ChoiceStyle(
                                    borderRadius: BorderRadius.circular(5),
                                    padding: EdgeInsets.symmetric(
                                      horizontal: displayWidth(context) * 0.07,
                                      vertical: displayHeight(context) * 0.015,
                                    ),
                                    // elevation: 2,
                                    // borderColor: Colors.white,
                                    labelStyle: GoogleFonts.poppins(
                                      color: Colors.grey[800],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                    )),
                                value: tag,
                                onChanged: (val) => setState(() => tag = val),
                                choiceItems: C2Choice.listFrom<int, String>(
                                  source: options,
                                  value: (i, v) => i,
                                  label: (i, v) => v,
                                ),
                              ),
                              Divider(),
                              SizedBox(
                                height: displayHeight(context) * 0.02,
                              ),
                              Text(
                                "Mode of Delivery",
                                style: GoogleFonts.poppins(
                                  color: Colors.grey[800],
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              ChipsChoice<int>.single(
                                padding: EdgeInsets.only(
                                    top: displayHeight(context) * 0.004),
                                choiceActiveStyle: C2ChoiceStyle(),
                                choiceStyle: C2ChoiceStyle(
                                    borderRadius: BorderRadius.circular(5),
                                    padding: EdgeInsets.symmetric(
                                      horizontal: displayWidth(context) * 0.07,
                                      vertical: displayHeight(context) * 0.015,
                                    ),
                                    // elevation: 2,
                                    // borderColor: Colors.white,
                                    labelStyle: GoogleFonts.poppins(
                                      color: Colors.grey[800],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                    )),
                                value: tag,
                                onChanged: (val) => setState(() => tag = val),
                                choiceItems: C2Choice.listFrom<int, String>(
                                  source: options,
                                  value: (i, v) => i,
                                  label: (i, v) => v,
                                ),
                              ),
                              Divider(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
