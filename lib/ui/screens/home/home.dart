import 'package:eceb/ui/widgets/home/appbar.dart';
import 'package:eceb/ui/widgets/home/oncall_doctors.dart';
import 'package:eceb/ui/widgets/home/register_baby.dart';
import 'package:eceb/ui/widgets/home/summary.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Appbar(
              title: "ECEB",
            ),
            Summary(),
            SizedBox(
              height: displayHeight(context) * 0.02,
            ),
            RegisterBaby(),
            OnCallDoctors(),
          ],
        ),
      ),
    );
  }
}
