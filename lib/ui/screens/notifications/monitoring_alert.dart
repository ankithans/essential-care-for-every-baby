import 'package:eceb/ui/widgets/notifications/notification_card.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MonitoringAlert extends StatefulWidget {
  @override
  _MonitoringAlertState createState() => _MonitoringAlertState();
}

class _MonitoringAlertState extends State<MonitoringAlert> {
  var data = [
    {
      "value": false,
      "color": Color(0xff38934B),
      "parent": "Oni",
      "time": "11:06 AM",
      "status": "Normal",
    },
    {
      "value": true,
      "color": Color(0xffE2574C),
      "parent": "Ada",
      "time": "10:56 AM",
      "status": "Danger",
    },
    {
      "value": true,
      "color": Color(0xffF9DB18),
      "parent": "Teka",
      "time": "11:06 AM",
      "status": "Normal",
    },
    {
      "value": true,
      "color": Color(0xff38934B),
      "parent": "Evi",
      "time": "11:06 AM",
      "status": "Normal",
    },
    {
      "value": false,
      "color": Color(0xff38934B),
      "parent": "Oni",
      "time": "11:06 AM",
      "status": "Normal",
    },
    {
      "value": false,
      "color": Color(0xffE2574C),
      "parent": "Ada",
      "time": "10:56 AM",
      "status": "Danger",
    },
    {
      "value": false,
      "color": Color(0xffF9DB18),
      "parent": "Teka",
      "time": "11:06 AM",
      "status": "Normal",
    },
    {
      "value": false,
      "color": Color(0xff38934B),
      "parent": "Evi",
      "time": "11:06 AM",
      "status": "Normal",
    },
    {
      "value": false,
      "color": Color(0xff38934B),
      "parent": "Oni",
      "time": "11:06 AM",
      "status": "Normal",
    },
    {
      "value": false,
      "color": Color(0xffE2574C),
      "parent": "Ada",
      "time": "10:56 AM",
      "status": "Danger",
    },
    {
      "value": false,
      "color": Color(0xffF9DB18),
      "parent": "Teka",
      "time": "11:06 AM",
      "status": "Normal",
    },
    {
      "value": false,
      "color": Color(0xff38934B),
      "parent": "Evi",
      "time": "11:06 AM",
      "status": "Normal",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: displayHeight(context) * 0.02,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: displayWidth(context) * 0.08,
              ),
              child: Text(
                "Today",
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.black87,
                ),
              ),
            ),
            SizedBox(
              height: displayHeight(context) * 0.02,
            ),
            SizedBox(
              child: ListView.separated(
                  separatorBuilder: (context, index) {
                    return Divider(
                      color: Colors.grey,
                    );
                  },
                  padding: EdgeInsets.only(top: displayHeight(context) * 0.01),
                  itemCount: data.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return NotificationCard(
                      color: data[index]["color"],
                      parent: data[index]["parent"],
                      time: data[index]["time"],
                      value: data[index]["value"],
                      status: data[index]["status"],
                      onChanged: (bool value) {
                        setState(() {
                          // value = !data[index]["value"];
                          data[index]["value"] = !data[index]["value"];
                        });
                      },
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
