import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MonitoringAlertNotificationCard extends StatefulWidget {
  final bool value;
  final Color color;
  final String parent;
  final String time;
  final String status;
  final Function onChanged;

  const MonitoringAlertNotificationCard({
    Key key,
    this.value,
    this.color,
    this.parent,
    this.time,
    this.status,
    this.onChanged,
  }) : super(key: key);

  @override
  _MonitoringAlertNotificationCardState createState() =>
      _MonitoringAlertNotificationCardState();
}

class _MonitoringAlertNotificationCardState
    extends State<MonitoringAlertNotificationCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CheckboxListTile(
        secondary: CircleAvatar(
          backgroundColor: widget.color,
        ),
        subtitle: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Location: Prenatal Ward",
                  style: GoogleFonts.poppins(
                    // color: Colors.white,
                    fontSize: 9,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  "EAT ${widget.time}",
                  style: GoogleFonts.poppins(
                    // color: Colors.white,
                    fontSize: 9,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Status Changed",
                  style: GoogleFonts.poppins(
                    color: Colors.black87,
                    fontSize: 10,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  widget.status,
                  style: GoogleFonts.poppins(
                    color:
                        widget.status == "Danger" ? Colors.red : Colors.black87,
                    fontSize: 10,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ],
        ),
        title: Text(
          "Baby of ${widget.parent}",
          style: GoogleFonts.poppins(
            color: Colors.black87,
            fontSize: 12,
            fontWeight: FontWeight.w500,
          ),
        ),
        onChanged: widget.onChanged,
        value: widget.value,
      ),
    );
  }
}
