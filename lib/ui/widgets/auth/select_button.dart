import 'package:eceb/utils/colors_helpers.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SelectButton extends StatelessWidget {
  final String title;
  final Function onPressed;
  const SelectButton({
    Key key,
    this.title,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 0.5,
      // color: Colors.white,
      borderRadius: BorderRadius.all(
        Radius.circular(20),
      ),
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          shadowColor: Colors.white,
          primary: Colors.white,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
        ),
        onPressed: onPressed,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: displayWidth(context) * 0.28,
            vertical: displayHeight(context) * 0.05,
          ),
          child: Text(
            title,
            style: GoogleFonts.poppins(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Color(0xff3082CC),
            ),
          ),
        ),
      ),
    );
  }
}
