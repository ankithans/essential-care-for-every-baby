import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
                style: GoogleFonts.poppins(
                  color: Colors.black,
                  fontSize: 11,
                ),
                text: "By continuing, you agree to our "),
            TextSpan(
                style: GoogleFonts.poppins(
                  color: Colors.blue,
                  fontSize: 11,
                ),
                text: "Privacy Policies, Data Use "),
            TextSpan(
                style: GoogleFonts.poppins(
                  color: Colors.black,
                  fontSize: 11,
                ),
                text: "including our "),
            TextSpan(
                style: GoogleFonts.poppins(
                  color: Colors.blue,
                  fontSize: 11,
                ),
                text: "Cookies Use"),
          ],
        ),
      ),
    );
  }
}
