import 'package:eceb/utils/colors_helpers.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AuthHeaderLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: displayWidth(context),
      height: displayHeight(context) * 0.58,
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      child: Column(
        children: [
          SizedBox(
            height: displayHeight(context) * 0.13,
          ),
          Image.asset(
            "assets/images/logo.png",
            width: displayWidth(context) * 0.44,
          ),
          SizedBox(
            height: displayHeight(context) * 0.03,
          ),
          Text(
            "ECEB",
            style: GoogleFonts.poppins(
              color: Colors.white,
              fontSize: 82,
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      ),
    );
  }
}
