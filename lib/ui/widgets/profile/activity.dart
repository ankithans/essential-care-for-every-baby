import 'package:eceb/ui/widgets/home/summary_card.dart';
import 'package:eceb/ui/widgets/list_babies/sort_dropdown.dart';
import 'package:eceb/ui/widgets/profile/activity_card.dart';
import 'package:eceb/utils/colors_helpers.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Activity extends StatefulWidget {
  @override
  _ActivityState createState() => _ActivityState();
}

class _ActivityState extends State<Activity> {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(10),
      elevation: 1,
      child: Stack(
        children: [
          Container(
            width: displayWidth(context),
            // height: displayHeight(context) * 0.25,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            child: Column(
              children: [
                SizedBox(
                  height: displayHeight(context) * 0.01,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: displayWidth(context) * 0.05,
                        ),
                        Text(
                          "Activity",
                          style: GoogleFonts.poppins(
                            color: Colors.black87,
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        SortDropDown(),
                        SizedBox(
                          width: displayWidth(context) * 0.05,
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: displayHeight(context) * 0.01,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: displayWidth(context) * 0.01,
                    ),
                    SummaryCard(
                      title: "Registered",
                      number: "5",
                      onPressed: () {},
                      image: "assets/images/admit.png",
                    ),
                    SummaryCard(
                      title: "Diagnosed",
                      number: "6",
                      onPressed: () {},
                      image: "assets/images/discharged.png",
                    ),
                    SummaryCard(
                      title: "Discharged",
                      number: "3",
                      onPressed: () {},
                      image: "assets/images/highrisk.png",
                    ),
                  ],
                ),
                SizedBox(
                  height: displayHeight(context) * 0.02,
                ),
                SizedBox(
                  child: ListView.separated(
                    padding: EdgeInsets.all(0),
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return ActivityCard(
                        title:
                            "Baby 1 of Oni registered at the Oost natal ward at 11:53 AM",
                        date: "19/05/2021",
                        time: "11:53 AM",
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider(
                        color: Colors.grey,
                      );
                    },
                    itemCount: 6,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
