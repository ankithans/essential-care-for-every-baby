import 'package:eceb/ui/widgets/home/doctor_avater.dart';
import 'package:eceb/ui/widgets/home/summary_card.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OnCallDoctors extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: displayWidth(context) * 0.06,
        top: displayHeight(context) * 0.02,
      ),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "On-Call Doctors",
              style: GoogleFonts.poppins(
                color: Colors.grey,
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(
              height: displayHeight(context) * 0.014,
            ),
            Row(
              children: [
                DoctorAvatar(
                  image: "assets/images/nurse1.png",
                  name: "Andrea",
                  status: "Online",
                ),
                DoctorAvatar(
                  image: "assets/images/nurse2.png",
                  name: "Kim",
                  status: "Online",
                ),
                DoctorAvatar(
                  image: "assets/images/nurse3.png",
                  name: "Jane",
                  status: "Online",
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
