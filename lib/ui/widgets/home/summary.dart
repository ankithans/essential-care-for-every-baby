import 'package:eceb/ui/widgets/home/summary_card.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Summary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: displayWidth(context) * 0.06,
        top: displayHeight(context) * 0.02,
      ),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Summary of 24 hours",
              style: GoogleFonts.poppins(
                color: Colors.grey,
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(
              height: displayHeight(context) * 0.01,
            ),
            Row(
              children: [
                SummaryCard(
                  image: "assets/images/admit.png",
                  title: "Admitted:",
                  number: "14",
                  onPressed: () {
                    Navigator.pushNamed(context, "/listbabies");
                  },
                ),
                SummaryCard(
                  image: "assets/images/discharged.png",
                  title: "Discharged:",
                  number: "20",
                  onPressed: () {
                    Navigator.pushNamed(context, "/listbabies");
                  },
                ),
                SummaryCard(
                  image: "assets/images/highrisk.png",
                  title: "High Risk:",
                  number: "5",
                  onPressed: () {
                    Navigator.pushNamed(context, "/listbabies");
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
