import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailsTextBox extends StatelessWidget {
  final String placeholder;
  final TextEditingController textEditingController;
  final TextInputType textInputType;
  final bool obscure;

  const DetailsTextBox({
    Key key,
    this.placeholder,
    this.textEditingController,
    this.textInputType,
    this.obscure,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        // horizontal: displayWidth(context) * 0.1,
        vertical: displayHeight(context) * 0.01,
      ),
      child: TextField(
        obscureText: obscure,
        textAlign: TextAlign.left,
        controller: textEditingController,
        keyboardType: textInputType,
        decoration: InputDecoration(
          hintText: placeholder,
          hintStyle: GoogleFonts.poppins(
            fontSize: 13,
            color: Colors.grey[500],
            fontWeight: FontWeight.w500,
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: const BorderSide(color: Colors.grey, width: 0.0),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.solid,
              color: Colors.grey[100],
            ),
          ),
          contentPadding: EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 10,
          ),
          // fillColor: colorSearchBg,
        ),
      ),
    );
  }
}
