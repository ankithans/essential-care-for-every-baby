import 'package:eceb/utils/colors_helpers.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RegisterBaby extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        padding: EdgeInsets.all(0),
      ),
      onPressed: () {
        Navigator.pushNamed(context, '/registerBaby');
      },
      child: Container(
        width: displayWidth(context),
        height: displayHeight(context) * 0.2,
        decoration: BoxDecoration(
          color: primaryColor,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: displayWidth(context) * 0.1,
            vertical: displayHeight(context) * 0.04,
          ),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  width: displayWidth(context) * 0.002,
                ),
                Image.asset(
                  "assets/images/note.png",
                  width: 42,
                ),
                Text(
                  "To Register a Baby",
                  style: GoogleFonts.poppins(
                    color: Colors.black87,
                    fontSize: 19,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  width: displayWidth(context) * 0.002,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
