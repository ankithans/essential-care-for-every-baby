import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DoctorAvatar extends StatelessWidget {
  final String name;
  final String status;
  final String image;

  const DoctorAvatar({Key key, this.name, this.status, this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12.0),
      child: Container(
        child: Column(
          children: [
            CircleAvatar(
              radius: 40,
              backgroundImage: AssetImage(image),
            ),
            SizedBox(
              height: displayHeight(context) * 0.01,
            ),
            Text(
              name,
              style: GoogleFonts.poppins(
                color: Colors.black87,
                fontSize: 15,
                fontWeight: FontWeight.w600,
              ),
            ),
            Text(
              status,
              style: GoogleFonts.poppins(
                color: Colors.blue[700],
                fontSize: 10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
