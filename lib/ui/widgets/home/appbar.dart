import 'package:eceb/utils/colors_helpers.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Appbar extends StatelessWidget {
  final String title;

  const Appbar({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: displayWidth(context),
      height: displayHeight(context) * 0.17,
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(top: displayHeight(context) * 0.03),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            // IconButton(
            //   icon: Icon(
            //     Icons.menu,
            //     color: Colors.white,
            //     size: 30,
            //   ),
            //   onPressed: () {},
            // ),
            // SizedBox(
            //     // width: displayWidth(context) * 0.05,
            //     ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: displayHeight(context) * 0.04,
                    ),
                    Text(
                      title,
                      style: GoogleFonts.poppins(
                        color: Colors.white,
                        fontSize: 28,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      " ID: ****124",
                      style: GoogleFonts.poppins(
                        color: Colors.white,
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width: displayWidth(context) * 0.1,
                ),
                title == "List of Babies"
                    ? SizedBox(
                        width: displayWidth(context) * 0.05,
                      )
                    : SizedBox(
                        width: displayWidth(context) * 0.35,
                      ),
                Padding(
                  padding: EdgeInsets.only(top: displayHeight(context) * 0.015),
                  child: Image.asset(
                    "assets/images/logo.png",
                    width: 60,
                  ),
                ),
                // SizedBox(
                //   width: displayWidth(context) * 0.1,
                // ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
