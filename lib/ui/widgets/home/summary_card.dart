import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SummaryCard extends StatelessWidget {
  final String title;
  final String number;
  final String image;
  final Function onPressed;

  const SummaryCard({
    Key key,
    this.title,
    this.number,
    this.image,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      style: TextButton.styleFrom(
        padding: EdgeInsets.all(0),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: displayWidth(context) * 0.02,
          vertical: displayHeight(context) * 0.015,
        ),
        child: Material(
          borderRadius: BorderRadius.circular(10),
          elevation: 10,
          shadowColor: Colors.white,
          child: Container(
            width: displayWidth(context) * 0.26,
            height: displayHeight(context) * 0.16,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: displayHeight(context) * 0.02),
                  child: Image.asset(
                    image,
                    width: 20,
                  ),
                ),
                SizedBox(
                  height: displayHeight(context) * 0.01,
                ),
                Text(
                  title,
                  style: GoogleFonts.poppins(
                    color: Colors.black87,
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: displayHeight(context) * 0.005,
                ),
                Text(
                  number,
                  style: GoogleFonts.poppins(
                    color: Colors.black87,
                    fontSize: 22,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
