import 'package:eceb/utils/colors_helpers.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class BabyCard extends StatelessWidget {
  final String parent;
  final bool female;
  final Color color;

  const BabyCard({Key key, this.parent, this.female, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: displayHeight(context) * 0.01,
        top: displayHeight(context) * 0.01,
      ),
      child: Material(
        elevation: 1,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
        child: Container(
          // width: displayWidth(context),
          // height: displayHeight(context) * 0.,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10),
              topLeft: Radius.circular(40),
              topRight: Radius.circular(40),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: displayWidth(context),
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: displayWidth(context) * 0.04,
                  vertical: displayHeight(context) * 0.005,
                ),
                child: Text(
                  "22 Minutes from Birth",
                  style: GoogleFonts.poppins(
                    color: Colors.black87,
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              SizedBox(
                height: displayHeight(context) * 0.01,
              ),
              Padding(
                padding: EdgeInsets.only(left: 12.0, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                style: GoogleFonts.poppins(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600),
                                text: "Baby",
                              ),
                              TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black87,
                                  fontSize: 11,
                                ),
                                text: "  of  ",
                              ),
                              TextSpan(
                                style: GoogleFonts.poppins(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600),
                                text: parent,
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black87,
                                  fontSize: 11,
                                ),
                                text: "Location: ",
                              ),
                              TextSpan(
                                style: GoogleFonts.poppins(
                                    color: Colors.black,
                                    fontSize: 11,
                                    fontWeight: FontWeight.w600),
                                text: "Prenatal Ward",
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Column(
                          children: [
                            female
                                ? Icon(MdiIcons.humanFemale)
                                : Icon(MdiIcons.humanMale),
                            Text(
                              female ? "Female" : "Male",
                              style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontSize: 11,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: displayWidth(context) * 0.03,
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          color: Colors.black,
                          size: 28,
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
