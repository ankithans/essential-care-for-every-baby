import 'package:eceb/ui/widgets/list_babies/color_baby_card.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PastRegistered extends StatelessWidget {
  var data = [
    {
      "parent": "Riya",
      "female": false,
      "color": Color(0xffF9D6D7),
      "backColor": Color(0xffEA6165),
    },
    {
      "parent": "Niya",
      "female": true,
      "color": Color(0xffFDF2BB),
      "backColor": Color(0xffFED604),
    },
    {
      "parent": "Adisa",
      "female": false,
      "color": Color(0xffCBE2CF),
      "backColor": Color(0xff3D9A4F),
    },
    {
      "parent": "Oni",
      "female": false,
      "color": Color(0xffF9D6D7),
      "backColor": Color(0xffEA6165),
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: displayHeight(context) * 0.02,
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: displayWidth(context) * 0.08,
            ),
            child: Text(
              "Past Registered",
              style: GoogleFonts.poppins(
                fontSize: 17,
                fontWeight: FontWeight.w700,
                color: Colors.black87,
              ),
            ),
          ),
          SizedBox(
            child: ListView.builder(
                padding: EdgeInsets.only(top: displayHeight(context) * 0.01),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return ColorBabyCard(
                    parent: data[index]["parent"],
                    female: data[index]["female"],
                    color: data[index]["color"],
                    backColor: data[index]["backColor"],
                  );
                }),
          ),
        ],
      ),
    );
  }
}
