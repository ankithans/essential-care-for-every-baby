import 'package:eceb/ui/widgets/list_babies/baby_card.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RecentlyAdded extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.symmetric(
      //   horizontal: displayWidth(context) * 0.08,
      // ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: displayWidth(context) * 0.08,
            ),
            child: Text(
              "Recently Added",
              style: GoogleFonts.poppins(
                fontSize: 17,
                fontWeight: FontWeight.w700,
                color: Colors.black87,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: displayWidth(context) * 0.06,
            ),
            child: SizedBox(
              child: ListView.builder(
                  padding: EdgeInsets.only(top: displayHeight(context) * 0.01),
                  itemCount: 2,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return BabyCard(
                      parent: "Oni",
                      female: true,
                      color: Color(0xffDFE6F1),
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }
}
