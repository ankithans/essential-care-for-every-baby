import 'package:eceb/utils/colors_helpers.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SortDropDown extends StatefulWidget {
  SortDropDown({Key key}) : super(key: key);

  @override
  _SortDropDownState createState() => _SortDropDownState();
}

/// This is the private State class that goes with SortDropDown.
class _SortDropDownState extends State<SortDropDown> {
  String dropdownValue = 'One';

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10),
      width: displayWidth(context) * 0.21,
      height: displayHeight(context) * 0.036,
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      child: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: primaryColor,
        ),
        child: DropdownButton<String>(
            hint: Text(
              "Sort By",
              style: GoogleFonts.poppins(
                color: Colors.white,
                fontSize: 11,
                fontWeight: FontWeight.w500,
              ),
            ),
            value: dropdownValue,
            icon: Icon(
              Icons.keyboard_arrow_down,
              size: 21,
              color: Colors.white,
            ),
            iconSize: 42,
            underline: SizedBox(),
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
              });
            },
            items: <String>['One', 'Two', 'Three', 'Four']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(
                  value,
                  style: GoogleFonts.poppins(
                    color: Colors.white,
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              );
            }).toList()),
      ),
    );
  }
}
