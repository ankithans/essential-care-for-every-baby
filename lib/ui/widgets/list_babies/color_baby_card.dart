import 'package:eceb/ui/widgets/list_babies/baby_card.dart';
import 'package:eceb/utils/size_helpers.dart';
import 'package:flutter/material.dart';

class ColorBabyCard extends StatelessWidget {
  final String parent;
  final bool female;
  final Color color;
  final Color backColor;

  const ColorBabyCard({
    Key key,
    this.parent,
    this.female,
    this.color,
    this.backColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: displayHeight(context) * 0.005,
        top: displayHeight(context) * 0.01,
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: displayWidth(context) * 0.06,
          vertical: displayHeight(context) * 0.01,
        ),
        color: backColor,
        width: displayWidth(context),
        child: BabyCard(
          parent: parent,
          female: female,
          color: color,
        ),
      ),
    );
  }
}
