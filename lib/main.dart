import 'package:eceb/ui/screens/auth/login_facility.dart';
import 'package:eceb/ui/screens/auth/login_individual.dart';
import 'package:eceb/ui/screens/auth/select.dart';
import 'package:eceb/ui/screens/home/home_page.dart';
import 'package:eceb/ui/screens/home/register.dart';
import 'package:eceb/ui/screens/list_babies/list_babies.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      title: 'Essential Care for Every Baby',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => Select(),
        '/loginindividual': (context) => LoginIndividual(),
        '/facility': (context) => LoginFacility(),
        '/homepage': (context) => HomePage(),
        '/listbabies': (context) => ListBabies(),
        '/registerBaby': (context) => Register(),
      },
    );
  }
}
