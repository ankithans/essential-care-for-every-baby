# Essential Care for Every Baby (WIP 🚧)
> This is the task for GSOC 2021 LibreHealth


## 🔗 Links
- [Android APK](https://gitlab.com/ankithans/essential-care-for-every-baby/-/raw/master/latest-release-apk/app-armeabi-v7a-release.apk)
- [Adobe XD Desgin](https://xd.adobe.com/view/910aed7c-fcb6-466a-91c4-664342e98e4c-c459/?fullscreen)

## ❓ About
> The Essential Care for Every Baby (ECEB) educational and training program, developed by the American Academy of Pediatrics, provides knowledge, skills, and competencies to nurses and doctors in low/middle-income settings so that they can provide life-saving care to newborns from birth through 24 hours postnatal.

## 💻 Samples

<img src="https://gitlab.com/ankithans/essential-care-for-every-baby/-/raw/master/mockups/image4.jpg" width="250"> &nbsp;&nbsp;&nbsp;&nbsp; <img src="https://gitlab.com/ankithans/essential-care-for-every-baby/-/raw/master/mockups/image6.jpg" width="250" style="float:right"> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <img src="https://gitlab.com/ankithans/essential-care-for-every-baby/-/raw/master/mockups/image7.jpg" width="250">

<img src="https://gitlab.com/ankithans/essential-care-for-every-baby/-/raw/master/mockups/image9.jpg" width="250"> &nbsp;&nbsp;&nbsp;&nbsp; <img src="https://gitlab.com/ankithans/essential-care-for-every-baby/-/raw/master/mockups/image3.jpg" width="250" style="float:right"> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <img src="https://gitlab.com/ankithans/essential-care-for-every-baby/-/raw/master/mockups/image1.jpg" width="250">

<!-- <img src="https://gitlab.com/ankithans/essential-care-for-every-baby/-/raw/master/mockups/image1.jpg" width="250"> &nbsp;&nbsp;&nbsp;&nbsp; <img src="https://gitlab.com/ankithans/essential-care-for-every-baby/-/raw/master/mockups/image2.jpg" width="250" style="float:right"> -->


## 🍧 Design link
https://xd.adobe.com/view/910aed7c-fcb6-466a-91c4-664342e98e4c-c459/?fullscreen


## 💡 Features
- Ability to track multiple babies at a time
- Time-stamping births to generate an essential newborn care clock for each baby being tracked in the app
- Automated classification of babies health status, based on results from observations and assessments that are conducted during the first 2 hours after birth 
- Automated advice on management of babies, based on their classification 
- Reminders/alerts to provide recommended essential newborn care interventions 
- If particular interventions are not delivered on time, or at all, capture of information regarding why this essential newborn care was late, or missed

## 👣 Steps to run the project
- clone the repo in your local machine
- do ```flutter pub get``` in root directory
- do ```flutter run``` in the root and your app will start with no issues

## 📍 Dependencies
- flutter_bloc
- google_fonts
- material_design_icons_flutter
- gitlab CI/CD

## 👷 Contributors
- [Ankit Hans](https://gitlab.com/ankithans)
